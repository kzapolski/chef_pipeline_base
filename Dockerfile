FROM ubuntu:18.04
RUN apt update -y
RUN apt install ruby ruby-dev zlib1g-dev liblzma-dev wget build-essential git curl jq -y

# RUN wget https://packages.chef.io/files/stable/chefdk/2.3.4/ubuntu/16.04/chefdk_2.3.4-1_amd64.deb
#RUN wget https://packages.chef.io/files/stable/chefdk/3.1.0/ubuntu/16.04/chefdk_3.1.0-1_amd64.deb
RUN wget https://packages.chef.io/files/current/chefdk/4.6.25/ubuntu/18.04/chefdk_4.6.25-1_amd64.deb
# RUN dpkg -i chefdk_2.3.4-1_amd64.deb
# RUN dpkg -i chefdk_3.1.0-1_amd64.deb
RUN dpkg -i chefdk_4.6.25-1_amd64.deb

RUN gem install knife-block
RUN gem install stove
RUN gem install foodcritic
RUN gem install rake
RUN gem install rubocop
RUN gem install cookstyle
RUN gem install chef

